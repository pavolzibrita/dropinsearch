# The default_config module automatically gets imported by Appconfig, if it
# exists. See https://pypi.python.org/pypi/flask-appconfig for details.

# Note: Don't *ever* do this in a real app. A secret key should not have a
#       default, rather the app should fail if it is missing. For the sample
#       application, one is provided for convenience.
SECRET_KEY = 'sdlfhas[fpuw rlnhfjw09rqworh;trp9yPOIATYWRP(YQ#[9y'

# !! Does not work right away with app engine
DEBUG = False

# Use *.min.* versions
BOOTSTRAP_USE_MINIFIED = False

# Because we're security-conscious developers, we also hard-code disabling
# the CDN support (this might become a default in later versions):
BOOTSTRAP_SERVE_LOCAL = True
