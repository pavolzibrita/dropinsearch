
from flask_bootstrap import __version__ as FLASK_BOOTSTRAP_VERSION
from flask_bootstrap.nav import BootstrapRenderer
from flask_nav import Nav
from flask_nav.elements import Navbar, Subgroup, Link, Separator, Text, View


# To keep things clean, we keep our Flask-Nav instance in here. We will define
# frontend-specific navbars in the respective frontend, but it is also possible
# to put share navigational items in here.
nav = Nav()

class DropinNavRenderer(BootstrapRenderer):
    def visit_Navbar(self, node):
        tag = BootstrapRenderer.visit_Navbar(self, node)
        tag['class'] = 'navbar navbar-inverse navbar-fixed-top'
        tag[0]['class'] = 'container'
        return tag 
    
# We're adding a navbar as well through flask-navbar. In our example, the
# navbar has an usual amount of Link-Elements, more commonly you will have a
# lot more View instances.
nav.register_element('admin_top', Navbar(
    View('Drop-in search', 'frontend.index'),
    View('Home', 'admin.index'),
    Subgroup(
        'Admin',
        View('List providers', 'admin.list_providers'),
        View('New provider', 'admin.new_provider'),
        View('List all events', 'admin.list_events'),
    ),
    Subgroup(
        'Docs',
        Link('Flask-Bootstrap', 'http://pythonhosted.org/Flask-Bootstrap'),
        Link('Flask-AppConfig', 'https://github.com/mbr/flask-appconfig'),
        Link('Flask-Debug', 'https://github.com/mbr/flask-debug'),
        Separator(),
        Text('Bootstrap'),
        Link('Getting started', 'http://getbootstrap.com/getting-started/'),
        Link('CSS', 'http://getbootstrap.com/css/'),
        Link('Components', 'http://getbootstrap.com/components/'),
        Link('Javascript', 'http://getbootstrap.com/javascript/'),
        Link('Customize', 'http://getbootstrap.com/customize/'),
    ),
    Text('Using Flask-Bootstrap {}'.format(FLASK_BOOTSTRAP_VERSION)),
))

nav.register_element('frontend_top', Navbar(
    View('Drop-in search', 'frontend.index'),
    Link('Home', '#search'),
    Link('Results', '#results'),
    Link('About', '#about')
))
