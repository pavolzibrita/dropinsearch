'''
Created on Dec 11, 2015

This module is for handling backend tasks, that take long
time and are invoked using task queue from other blueprints

@author: pavol
'''
import logging

from google.appengine.api import taskqueue

from flask.blueprints import Blueprint
from flask.helpers import url_for
from fts import fts
from model.events import EventProvider, Event, P_ICAL, P_TORONTOPARKS
from parsers.icalparser import ical_to_events
from parsers.torontoparks import torontoparks_to_events, \
    torontoparks_to_providers
from parsers.utils import load_link


backend = Blueprint('backend', __name__, url_prefix='/backend')

@backend.route('/process_toronto_parks', methods=('GET', 'POST'))
def process_toronto_parks():
    url = 'http://www1.toronto.ca/parks/prd/facilities/recreationcentres/index.htm'
    st = load_link(url)
    providers = torontoparks_to_providers(st, url=url)
    for provider in EventProvider.query_toronto_providers():
        try:
            provider.key.delete()
            provider_changed(provider.key.id())
        except:
            logging.exception("Failed to delete provider: %s" % provider)
    for provider in providers:
        try:
            provider.put()
        except:
            logging.exception("Failed to save provider: %s" % provider)
    return 'Processed'

@backend.route('/process_provider/<int:provider_key>', methods=('GET', 'POST'))
def process_provider(provider_key):
    provider = EventProvider.get_by_id(provider_key)
    if not provider:
        deleted = _delete_providers_events(EventProvider.create_key(provider_key))
        if not deleted:
            logging.info('No events to delete for provider key: %s' % provider_key)
            return 'No events to delete for provider key: %s' % provider_key
        else:
            return '%d events deleted, for provider : %s' % (deleted, provider_key)
    
    if not provider.link:
        logging.warn('Provider does not have url to be processed as ical')
        return 'Provider does not have url to be processed as ical'

    if provider.processor == P_ICAL:
        process_ical_provider(provider)
    elif provider.processor == P_TORONTOPARKS:
        process_torontoparks_provider(provider) 
    else:
        logging.error('Do not know the processor: %s ' % provider.processor)
        return 'Do not know the processor: %s ' % provider.processor
    return 'Processed'

def _replace_events(provider, events):
    '''
    @param provider: EventProvider
    @param events: list
    '''
    _delete_providers_events(provider.key)
    _save_events(events)

def _delete_providers_events(provider_key):
    '''
    @param provider_key: google.appengine.ext.Key  
    @return: True if some events were deleted
    '''
    logging.info('Deleting all events for provider: %s' % provider_key)
    old_events = Event.query(ancestor=provider_key)
    events_deleted = 0
    for old_event in old_events:
        old_event.delete(fts)
        events_deleted += 1
    logging.info('Deleted %s events for provider: %s' % (events_deleted, provider_key))
    return events_deleted

def _save_events(events):
    # then store new entries
    for event in events:
        try:
            event.put(fts)
        except Exception:
            logging.exception("Failed to save event")
    logging.info("Saved %d events" % len(events))
        
def process_torontoparks_provider(provider):
    '''
    @param provider: EventProvider
    '''
    st = load_link(provider.link)
    events = torontoparks_to_events(st, provider)
    _replace_events(provider, events)

def process_ical_provider(provider):
    '''
    @param provider: EventProvider
    '''
    st = load_link(provider.link)
    events = ical_to_events(st, provider)
    _replace_events(provider, events)

def provider_changed(provider_key):
    '''
    @param provider_key: int
    '''
    taskqueue.add(url=url_for('backend.process_provider',
                              provider_key=provider_key),
                  params={})

@backend.route('/schedule_updates', methods=('GET', 'POST'))
def schedule_updates():
    for provider in EventProvider.query():
        provider_changed(provider.key.id())
    logging.info('All providers scheduled for update')
    return 'All providers scheduled for update'

@backend.route('/schedule_toronto_parks', methods=('GET', 'POST'))
def schedule_toronto_parks():
    taskqueue.add(
              url=url_for('backend.process_toronto_parks'),
              params={})
    logging.info('Scheduled Toronto parks recreation')
    return 'Scheduled Toronto parks recreation'

@backend.route('/delete_index', methods=('GET', 'POST'))
def delete_index():
    """Delete all the docs in the given index."""
    doc_index = fts

    # looping because get_range by default returns up to 100 documents at a time
    while True:
        # Get a list of documents populating only the doc_id field and extract the ids.
        document_ids = [document.doc_id
                        for document in doc_index.get_range(ids_only=True)]
        if not document_ids:
            break
        # Delete the documents for the given ids from the Index.
        logging.info("Delete %d indexes" % len(document_ids))
        doc_index.delete(document_ids)
    return 'Index successfully deleted'
