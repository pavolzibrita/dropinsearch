from datetime import datetime, timedelta
import logging

from google.appengine.api import search

from flask import Blueprint, render_template, flash
from fts import fts
from model.events import Event, get_subterms, EventProvider
from model.forms import SearchQueryForm, SearchQuery

frontend = Blueprint('frontend', __name__)

def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (
                getattr(form, field).label.text,
                error
            ), 'warning')
            
def find_events(docs, now, form_date):
    '''
    @param docs: the search documents
    @param now: datetime 
    '''
    events, later = [], []
    old, missing = 0, 0
    for doc in docs:
        try:
            eid, parent_id = doc.doc_id.split('_')
            eid = int(eid)
            parent_id = int(parent_id)
            event = Event.get_by_id(eid, EventProvider.create_key(parent_id))
            if event:
                if event.start_date > form_date:
                    if event.start_date.date() > now.date():
                        later.append(event)
                    else:
                        events.append(event)
                else:
                    old += 1
            else:
                missing += 1
        except:
            logging.exception("Could not get event for doc:%s" % doc)
    return events, later, old, missing

def get_now_with_timezone(timezone):
    tz = 0
    try:
        tz = int(timezone)
    except:
        pass
    return datetime.now() + timedelta(hours=tz)  # TODO and what about DST?
    

@frontend.route('/', methods=('GET', 'POST'))
def index():
    model = SearchQuery()
    form = SearchQueryForm(obj=model)
    if form.validate_on_submit():
        form.populate_obj(model)

        sort_exp = []
        
        userq = model.activity.strip()
        query = ' OR '.join([userq] + get_subterms(userq))
        
        if model.fromdate:
            date_expr = "start_date"
            query = "%s >= %s AND %s" % (date_expr, model.fromdate.strftime('%Y-%m-%d'), query)
            sortexpr = search.SortExpression(
                expression=date_expr,
                direction=search.SortExpression.ASCENDING,
                    default_value=datetime.now())
            sort_exp.append(sortexpr)
            
        if model.location_latlon:
            lat, lon = (model.location_latlon.lat, model.location_latlon.lon)
            if (lat != 0 or lon != 0):
                loc_expr = "distance(%s, geopoint(%f, %f))" % (
                    Event.LOCATION_LATLON, lat, lon)
                query = "%s < %f AND %s" \
                        % (loc_expr, 45000, query)
                sortexpr = search.SortExpression(
                    expression=loc_expr,
                    direction=search.SortExpression.ASCENDING, default_value=45001)
                sort_exp.append(sortexpr)
            
        sort_options = search.SortOptions(expressions=sort_exp)
        search_query = search.Query(
            query_string=query,
            options=search.QueryOptions(limit=100, sort_options=sort_options))

        search_results = fts.search(search_query)
        # This is a shifted utc date instead of using timezone aware date, so probably
        # it would be better to determine the timezone, but... works.
        clients_now = get_now_with_timezone(model.timezone)
        events, later_events, old, missing = find_events(search_results, clients_now, model.fromdate)
        logging.info("Returned %d events, %d later, %d old, %d missing for query %s" 
                     % (len(events), len(later_events), old, missing, query))
        return render_template('index.html',
                               form=form,
                               activity=form.activity.data,
                               location=form.location.data,
                               location_latlon=form.location_latlon.data,
                               events=events,
                               later_events=later_events)    
    flash_errors(form)
    return render_template('index.html', form=form)

