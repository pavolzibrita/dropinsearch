function mapLocations(latitude, longitude, input_address, input_latlon) {
	// we don't have currect possition
	if (latitude == 0 && longitude == 0) {
		$('input[name="' + input_address + '"]').val("Unable to get location");
		$('input[name="' + input_latlon + '"]')
				.val(latitude + ", " + longitude);
	} else {
		var latlng = {
			lat : latitude,
			lng : longitude
		};
		var geocoder = new google.maps.Geocoder;
		geocoder.geocode({
			'location' : latlng
		}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				if (results[1]) {
					$('input[name="' + input_address + '"]').val(
							results[1].formatted_address);
					$('input[name="' + input_latlon + '"]').val(
							latitude + ", " + longitude);
				} else {
					window.alert('No results found');
				}
			} else {
				window.alert('Geocoder failed due to: ' + status);
			}
		});
	}
}

function findLocation(input_address, input_latlon) {

	if ($('input[name="' + input_latlon + '"]').val() != "") {
		return;
	}

	if (navigator.geolocation) {
		navigator.geolocation
				.getCurrentPosition(
						function(position) {

							mapLocations(position.coords.latitude,
									position.coords.longitude, input_address,
									input_latlon);

						}, // next function is the error callback
						function(error) {
							switch (error.code) {
							case error.TIMEOUT:
								alert('Timeout');
								break;
							case error.POSITION_UNAVAILABLE:
								alert('Position unavailable; using fake data (0, 0).  If you are running from localhost, note that geosearch is not currently fully supported on the dev app server.');
								mapLocations(0, 0);
								break;
							case error.PERMISSION_DENIED:
								alert('Permission denied; using fake data.(-33.873038, 151.20563).  If you are running from localhost, note that geosearch is not currently fully supported on the dev app server.');
								mapLocations(0, 0);
								break;
							case error.UNKNOWN_ERROR:
								alert('Unknown error');
								break;
							}
						});
	} else {
		alert("Geolocation services are not supported by your browser or you do not have a GPS device in your computer. Using fake data.");
		mapLocations(0, 0, input_address, input_latlon);
	}
}

if ($('#location').length != 0) {
	$('#location').attr('autocomplete','off');
	$('#location_latlon').hide();
	var addressPicker = new AddressPicker();
    $('#location').typeahead({
		displayKey: 'description',
		source: addressPicker.ttAdapter(),
		displayText: function(item) { return item.description; },
		afterSelect: function(item) { addressPicker.updateMap(null, item); },
		matcher : function (item) { return true; }
    });
    // Listen for selected places result
    $(addressPicker).on('addresspicker:selected', function (event, result) {
      $('#location_latlon').val(result.lat() + ', ' + result.lng());
    });
}

if ($('#timezone').length != 0) {
	$('form').on('submit', function(e){
	    $('#timezone').val(-new Date().getTimezoneOffset()/60);
	});
}

if (!Number.prototype.pad) {
	Number.prototype.pad = function(size) {
	    var s = String(this);
	    while (s.length < (size || 2)) {s = "0" + s;}
	    return s;
	}
}

//First, checks if it isn't implemented yet.
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    result = this.replace(/{(\d+)d(\d+)}/g, function(match, number, padding) { 
        return typeof args[number] != 'undefined'
          ? args[number].pad(padding)
          : match
        ;
      });
    return result.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

$(document).ready(function() {
	if ($('#fromdate').length != 0 && $('#fromdate').val() == "") {
		// set a date, e.g. "2013-12-31T15:35"
		d = new Date();
		date_str = "{0d4}-{1d2}-{2d2}T{3d2}:{4d2}".format(d.getFullYear(), d.getMonth()+1, d.getDate(), d.getHours(), d.getMinutes());
	    $("#fromdate").val(date_str); 
	}
});