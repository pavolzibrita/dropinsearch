from datetime import datetime

from google.appengine.ext import ndb as db

from flask_wtf import Form
from wtforms.fields.core import DateTimeField
from wtforms.fields.simple import TextField, FileField, SubmitField, HiddenField
from wtforms.validators import Required, Email
from wtforms_appengine.fields import GeoPtPropertyField


# from wtforms.fields.core import DateField, FloatField, DecimalField, \
#     DateTimeField, BooleanField, IntegerField
# class SignupForm(Form):
#     name = TextField(u'Your name', validators=[Required()])
#     password = TextField(u'Your favorite password', validators=[Required()])
#     email = TextField(u'Your email address', validators=[Email()])
#     birthday = DateField(u'Your birthday')
# 
#     a_float = FloatField(u'A floating point number')
#     a_decimal = DecimalField(u'Another floating point number')
#     a_integer = IntegerField(u'An integer')
# 
#     now = DateTimeField(u'Current time',
#                         description='...for no particular reason')
#     sample_file = FileField(u'Your favorite file')
#     eula = BooleanField(u'I did not read the terms and conditions',
#                         validators=[Required('You must agree to not agree!')])
# 
#     submit = SubmitField(u'Signup')
class SearchQuery(db.Model):
    activity = db.StringProperty(indexed=False)
    location = db.StringProperty(indexed=False)
    location_latlon = db.GeoPtProperty(indexed=False)
    fromdate = db.DateTimeProperty(indexed=False, auto_now=True)
    
    # def __init__(self):
        # self.formdate = datetime.now()  # .strftime('%Y-%m-%dT%H:%M')
    
class SearchQueryForm(Form):
    activity = TextField(u'Drop-in', validators=[Required("Activity type is required")])
    location = TextField(u'near', validators=[])
    location_latlon = GeoPtPropertyField(u'Location_latlon', validators=[Required("Location is required")])
    fromdate = DateTimeField(u'starting', format='%Y-%m-%dT%H:%M')
    timezone = HiddenField()
    submit = SubmitField(u'Go!')
