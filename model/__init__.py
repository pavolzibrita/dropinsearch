from google.appengine.api import search
from google.appengine.ext import ndb as db

class GoogleAPIKey(db.Model):
    """Google API Keys"""
    secret = db.StringProperty(indexed=False)
    
    @staticmethod
    def create_key(key):
        return db.Key(GoogleAPIKey, key)
