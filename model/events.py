# import datetime
from string import lower

from google.appengine.api import search
from google.appengine.ext import ndb as db

from flask_wtf.form import Form
from wtforms.fields.core import SelectField
from wtforms.fields.simple import SubmitField
from wtforms_appengine.ndb import model_form

P_ICAL = 'P_ICAL'
P_MEETUP = 'P_MEETUP'
P_TORONTOPARKS = 'P_TORONTOPARKS'
EPROVIDERTYPES = [(P_ICAL, 'ICAL'), (P_MEETUP, 'MEETUP'), (P_TORONTOPARKS, 'TORONTO PARKS')]

class EventProvider(db.Model):
    """Event provider"""
    provider = db.StringProperty(indexed=False)
    link = db.StringProperty(indexed=False)
    location = db.StringProperty(indexed=False)
    location_latlon = db.GeoPtProperty(indexed=False)
    processor = db.StringProperty(indexed=False)
    
    @staticmethod
    def create_key(key):
        return db.Key(EventProvider, key)
    
    @staticmethod
    def model_form():
        modelled_form = model_form(EventProvider, Form, exclude=['processor'])
        modelled_form.processor = SelectField(u'Provider type', choices=EPROVIDERTYPES)
        return modelled_form
      
    @staticmethod
    def query_toronto_providers():
        providers = EventProvider.query()
        return (p for p in providers if p.processor == P_TORONTOPARKS)  

AddEventProviderForm = EventProvider.model_form()
AddEventProviderForm.submit = SubmitField(u'Add')

EditEventProviderForm = EventProvider.model_form() 
EditEventProviderForm.update = SubmitField(u'Update')
EditEventProviderForm.delete = SubmitField(u'Delete')

def get_subterms(summary):
        subterms = {'volley':['volley', 'volleyball'],
                    'hockey':['hockey'],
                    'futsal':['soccer'],
                    'soccer':['futsal'],
                    'shinny':['hockey']}
        low = lower(summary)
        terms = sum([v for k, v in subterms.iteritems() if k in low], [])
        return terms
        
class Event(db.Model):
    EVENT_ID = 'event_id'
    TYPE = 'type'
    LINK = 'link'
    START_DATE = 'start_date'
    END_DATE = 'end_date'
    LOCATION = 'location'
    LOCATION_LATLON = 'location_latlon'
    UPDATED = 'updated'
    """Event"""
    type = db.StringProperty(indexed=False)
    link = db.StringProperty(indexed=False)
    start_date = db.DateTimeProperty(indexed=False)
    end_date = db.DateTimeProperty(indexed=False)
    location = db.StringProperty(indexed=False)
    location_latlon = db.GeoPtProperty(indexed=False)
    
    def put(self, fts):
        super(Event, self).put()
        fields = [
          # search.TextField(name=Event.EVENT_ID, value=str(self.key.id())),
          search.TextField(name=Event.TYPE,
                           value=' '.join([self.type] + get_subterms(self.type))),
          # search.DateField(name=Event.UPDATED,
          #                 value=datetime.datetime.now().date()),
          search.DateField(name=Event.START_DATE,
                           value=self.start_date),
          search.DateField(name=Event.END_DATE,
                           value=self.end_date),
          # search.TextField(name=Event.LOCATION, value=self.location),
        ]
        if self.location_latlon:
            field = search.GeoField(name=Event.LOCATION_LATLON, value=
                          search.GeoPoint(self.location_latlon.lat,
                                          self.location_latlon.lon)) 
            fields.append(field)
        d = search.Document(doc_id=self._create_docid(), fields=fields)
        fts.put(d)

    def _create_docid(self):
        return '_'.join([str(self.key.id()), str(self.key.parent().id())])
                
    def delete(self, fts):
        '''
        @param fts: search.Index
        '''
        fts.delete(self._create_docid())
        self.key.delete()

AddEventForm = model_form(Event, Form)
AddEventForm.submit = SubmitField(u'Add')

EditEventForm = model_form(Event, Form)
EditEventForm.update = SubmitField(u'Update')
EditEventForm.delete = SubmitField(u'Delete')
