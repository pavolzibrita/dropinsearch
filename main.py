"""`main` is the top level module for your Flask application."""

import socket
import string

from admin import admin
from backend import backend
from flask import Flask
from flask_appconfig import AppConfig
from flask_bootstrap import Bootstrap
from flask_nav import register_renderer
from frontend import frontend
from nav import nav, DropinNavRenderer


app = Flask(__name__)

# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

# We use Flask-Appconfig here, but this is not a requirement
AppConfig(app, default_settings='default_config')

# Install our Bootstrap extension
Bootstrap(app)

# Our application uses blueprints as well; these go well with the
# application factory. We already imported the blueprint, now we just need
# to register it:
app.register_blueprint(frontend)
app.register_blueprint(backend)
app.register_blueprint(admin)

# We initialize the navigation as well
nav.init_app(app)

register_renderer(app, None, DropinNavRenderer)
    
@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, Nothing at this URL.', 404

@app.errorhandler(500)
def application_error(e):
    """Return a custom 500 error."""
    return 'Sorry, unexpected error: {}'.format(e), 500

@app.template_global()
def realhost():
    h = socket.gethostname()
    for host in ['localhost', 'appspot', 'Ciri']:
        if string.find(h, host) != -1:
            return False
    return True 

@app.template_global()
def host_name():
    return socket.gethostname()
