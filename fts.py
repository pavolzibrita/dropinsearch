'''
Created on Dec 11, 2015

Holds the full text search index that we want to use for ft searchings
@author: pavol
'''
from google.appengine.api import search


fts = search.Index(name='dropinevents')
