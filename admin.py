'''
Created on Dec 11, 2015

@author: pavol
'''
import logging

from google.appengine.api import taskqueue

from backend import provider_changed
from flask.blueprints import Blueprint
from flask.helpers import flash, url_for
from flask.templating import render_template
from frontend import flash_errors
from fts import fts
from model.events import EventProvider, AddEventProviderForm, \
    EditEventProviderForm, Event, AddEventForm, EditEventForm
from werkzeug.utils import redirect


admin = Blueprint('admin', __name__, url_prefix='/admin')

@admin.route('/')
def index():
    return render_template('admin.html')

@admin.route('/list_providers/')
def list_providers():
    providers = EventProvider.query()
    return render_template('list_providers.html', providers=providers)

@admin.route('/new_provider/', methods=('GET', 'POST'))
def new_provider():
    form = AddEventProviderForm()
    
    if form.validate_on_submit():
        model = EventProvider()
        form.populate_obj(model)
        key = model.put()
        provider_changed(key.id())
        flash("EventProvider inserted")
        return redirect(url_for(".list_providers"))
    flash_errors(form)
    return render_template("edit_provider.html", form=form)

@admin.route('/edit_provider/<int:key>', methods=('GET', 'POST'))
def edit_provider(key):
    model = EventProvider.get_by_id(key)
    form = EditEventProviderForm(obj=model) 
    if form.update.data and form.validate_on_submit():
        form.populate_obj(model)
        model.put()
        provider_changed(key)
        flash("EventProvider updated")
        return redirect(url_for(".edit_provider", key=key))
    if form.delete.data:
        model.key.delete()
        provider_changed(key)
        flash("EventProvider deleted")
        return redirect(url_for(".list_providers"))
    flash_errors(form)
    provider_key = EventProvider.create_key(key)
    events = Event.query(ancestor=provider_key)
    return render_template("edit_provider.html", form=form, events=events, provider_key=provider_key)

@admin.route('/list_events/')
def list_events():
    events = Event.query()
    return render_template('list_events.html', events=events)

@admin.route('/new_event/<int:provider_key>', methods=('GET', 'POST'))
def new_event(provider_key):
    form = AddEventForm()
    
    if form.validate_on_submit():
        model = Event(parent=EventProvider.create_key(provider_key))
        form.populate_obj(model)
        model.put(fts)
        flash("Event inserted")
        return redirect(url_for(".edit_provider", key=provider_key))
    flash_errors(form)
    return render_template("edit_event.html", form=form)

@admin.route('/edit_event/<int:key>/<int:parent_key>', methods=('GET', 'POST'))
def edit_event(key, parent_key):
    model = Event.get_by_id(key, EventProvider.create_key(parent_key))
    form = EditEventForm(obj=model) 
    if form.update.data and form.validate_on_submit():
        form.populate_obj(model)
        model.put(fts)
        flash("Event updated")
        return redirect(url_for(".edit_provider", key=parent_key))
    if form.delete.data and form.validate_on_submit():
        model.delete(fts)
        flash("Event deleted")
        return redirect(url_for(".edit_provider", key=parent_key))
    flash_errors(form)
    return render_template("edit_event.html", form=form)

@admin.route('/delete_index', methods=('GET', 'POST'))
def schedule_toronto_parks():
    taskqueue.add(
              url=url_for('backend.delete_index'),
              params={})
    flash(u"Scheduled deleting of complete index", 'message')
    logging.info('Scheduled deleting of complete index')
    return render_template('admin.html')