'''
Created on Dec 28, 2015

@author: pavol
'''
from datetime import timedelta, datetime
import logging
import urllib2

from google.appengine.ext.ndb.model import GeoPt

from dateutil import parser
from model import GoogleAPIKey
import pytz

google_maps_cache = None

def google_maps():
    global google_maps_cache
    if google_maps_cache is None:
        from geolocation.main import GoogleMaps
        secret = 'Dummy secret'
        apikeys = GoogleAPIKey.query()
        for apikey in apikeys:
            secret = apikey.secret
            break
        if secret == 'DUmmy secret':
            logging.error('Could not find Google API secret in datastore!')
        else:
            logging.info('Google API secret successfully retrieved from datastore!')
        google_maps_cache = GoogleMaps(api_key=secret)
    return google_maps_cache
    
def dt_to_utc(dt):
    if dt.tzinfo is None:
        return dt
    else:
        return dt.astimezone(pytz.utc).replace(tzinfo=None)
    
def get_start_end(day, from_to, tz, now=None, max_delta_days=30 * 6):
    def closer_date(day, day_time, year):
        day_string = "%s %s %d %s" % (day, day_time, year, tz)
        for i in [0, 1]:
            day_string = "%s %s %d %s" % (day, day_time, year + i, tz)
            try:
                pdate = dt_to_utc(parser.parse(day_string))
                if now - pdate < timedelta(days=max_delta_days):
                    return pdate
            except:
                logging.warn("Could not parse date:%s" % day_string)
        raise ValueError("Could not parse date: %s" % day_string)
    if now is None:
        now = datetime.now()
    start, end = from_to.split('-')
    start = start.strip().lower()
    end = end.strip().lower()
    if (end.endswith('pm') or end.endswith('am')) and \
        not (start.endswith('pm') or start.endswith('am')):
        start += end[-2:] 
    pstart = closer_date(day, start, now.year)
    pend = closer_date(day, end, now.year)
    return pstart, pend

def load_link(link):
    '''
    @param link: str 
    '''
    request = urllib2.Request(link)
    request.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36')
    opener = urllib2.build_opener()
    st = opener.open(request).read()
    return st

def location_2_latlon(location):
    '''
    @param location: str - location string
    '''
    location = google_maps().search(location=location)  # sends search to Google Maps.
    
    my_location = location.first()  # returns only first location.
    
    return GeoPt(lat=my_location.lat, lon=my_location.lng) if my_location else None
