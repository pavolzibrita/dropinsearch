'''
Created on Dec 28, 2015

@author: pavol
'''
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import logging
import os
import re

from model.events import Event, P_TORONTOPARKS, EventProvider
from parsers.utils import get_start_end, load_link, location_2_latlon


def torontoparks_to_events(st, parent, now=None, max_days=14):
    if now is None:
        now = datetime.now()
    soup = BeautifulSoup(st, 'html.parser')
    body = soup.find(id="pfrContentBody")
    
    ul = body.find('ul')
    if not ul:
        a = soup.find('a', attrs={'title':'TTC Schedules'})
        ul = a.find_parent('ul')
    if not ul:
        logging.error('Could not find location for provider:%s' % parent.key.id())
        return 'Could not find location for %s' % parent.key.id()
    location = '%s,%s' % (body.h1.get_text().strip(), ul.li.get_text().strip()) 
    
    all_events = []
    for tr in soup.find_all(id=re.compile("dropin_.*")):
        thead = tr.td.table.thead
        tbody = tr.td.table.tbody
        times = []
        meta_events = []
        for index, th in enumerate(thead.find_all('th')):
            times.append(th.get_text())
        for tr in tbody.find_all('tr'):
            event = []
            for index, td in enumerate(tr.find_all('td')):
                if index == 0:
                    event.append(td.get_text())  # get the header
                else:
                    event.append(td.get_text(separator=';'))  # get time(s)
            meta_events.append(event)
        for meta_event in meta_events:
            event_type = meta_event[0]
            for day, from_to_lines in zip(times[1:], meta_event[1:]):
                for from_to in from_to_lines.split(';'):
                    if from_to.strip() == "":
                        continue
                    try:
                        event = Event(parent=parent.key)
                        event.start_date, event.end_date = get_start_end(day, from_to, "EST", now=now)
                        if event.start_date - now > timedelta(days=max_days):
                            continue 
                        event.type = event_type
                        event.link = parent.link
                        event.location = location
                        event.location_latlon = parent.location_latlon
                        all_events.append(event)
                    except:
                        logging.warn("Could not parse event, %s, %s, %s" % (event_type, location, from_to))
    return all_events

def torontoparks_to_providers(st, url, now=None):
    if now is None:
        now = datetime.now()
    soup = BeautifulSoup(st, 'html.parser')
    nav = soup.find(id="pfrNavAlpha2")
    subpages = []
    for link in nav.find_all('a'):
        subpages.append(link.get('href'))
    result = []
    for page in subpages:
        sub_page = os.path.join(os.path.dirname(url), page)
        logging.info("Parsing sub-page: %s" % sub_page)
        content = load_link(sub_page)
        result.extend(torontoparks_page_to_providers(content, now=now))
    return result


def torontoparks_page_to_providers(st, now=None):
    if now is None:
        now = datetime.now()
    soup = BeautifulSoup(st, 'html.parser')
    # table_top = soup.find(_class="pfrBody")
    tbody = soup.find('tbody')
    results = []
    for row in tbody.find_all('tr'):
        cols = [td for td in row.find_all('td')]
        ahref = cols[0].find('a')
        ep = EventProvider()
        ep.link = "http://www1.toronto.ca:80" + ahref.get('href')
        ep.provider = ahref.get_text().strip()
        ep.location = cols[1].get_text().strip() + ", Toronto, ON"
        ep.location_latlon = location_2_latlon(ep.location)
        ep.processor = P_TORONTOPARKS 
        results.append(ep)
    return results
