'''
Created on Dec 10, 2015

@author: pavol
'''
import logging

from google.appengine.ext.ndb.model import GeoPt

from icalendar.cal import Calendar
from model.events import Event
from parsers.utils import dt_to_utc


def ical_to_events(st, parent):
    '''
    @param st: str
    @param parent: EventProvider
    '''
    events = []
    cal = Calendar.from_ical(st, multiple=False)
    for sub in cal.subcomponents:
        try:
            if sub.name != "VEVENT":
                continue
            
            event = Event(parent=parent.key)
            event.type = sub.get('SUMMARY').decode()
            event.start_date = dt_to_utc(sub.get('DTSTART').dt)
            event.end_date = dt_to_utc(sub.get('DTEND').dt)
            if sub.get('URL'): 
                event.link = sub.get('URL').decode()
            else:
                event.link = parent.link
            if sub.get('LOCATION'):  
                event.location = sub.get('LOCATION').decode()
            else:
                event.location = parent.location
            if sub.get('GEO'): 
                # Fix error in some calendars witch switched latitude/longitude....
                if (abs(sub.get('GEO').latitude) > 50):
                    event.location_latlon = GeoPt(sub.get('GEO').longitude, sub.get('GEO').latitude)
                else:
                    event.location_latlon = GeoPt(sub.get('GEO').latitude, sub.get('GEO').longitude)
            else:
                event.location_latlon = parent.location_latlon
            events.append(event)
        except Exception:
            logging.warn("Failed to process event: %s" % sub.to_ical())
    return events
