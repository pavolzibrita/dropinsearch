'''
Created on Dec 31, 2015

@author: pavol
'''
def assert_provider(p, link, name, location, location_latlon, processor):
    assert p.link == link
    assert p.provider == name
    assert p.location == location
    assert p.location_latlon == location_latlon
    assert p.processor == processor

def assert_event(event, location, link, location_latlon, start_date, end_date, type):
    assert event.location == location
    assert event.link == link
    assert event.location_latlon == location_latlon
    assert event.start_date == start_date
    assert event.end_date == end_date
    assert event.type == type

