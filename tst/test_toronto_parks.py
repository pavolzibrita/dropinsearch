from datetime import datetime
import pytest

from google.appengine.ext.ndb.model import GeoPt

from conftest import assert_provider, assert_event
from mock.mock import patch
from model.events import EventProvider, P_TORONTOPARKS
from parsers.torontoparks import torontoparks_to_events, \
    torontoparks_to_providers, torontoparks_page_to_providers
from parsers.utils import load_link


def test_toronto_parks_parsing():
    st = open('tst/toronto_parks.html').read()
    now = datetime(2015, 12, 28, 8)
    parent = EventProvider()
    parent.link = 'Parent'' link'
    parent.location_latlon = GeoPt(10, 10)
    
    events = torontoparks_to_events(st, parent, now=now)

    assert len(events) == 137
    event = events[0]
    assert_event(event,
        location=u'Antibes Community Centre,140 Antibes Dr M2R 3J3',
        link=parent.link,
        location_latlon=parent.location_latlon,
        start_date=datetime(2015, 12, 28, 13, 0),
        end_date=datetime(2015, 12, 28, 18, 0),
        type=u'Weight Room(16yrs and over)')
    
    
def test_broken_toronto_parks_provider():
    st = open('tst/broken_provider.html').read()
    now = datetime(2016, 01, 10, 8)
    parent = EventProvider()
    parent.link = 'Parent'' link'
    parent.location_latlon = GeoPt(10, 10)
    
    events = torontoparks_to_events(st, parent, now=now)

    assert len(events) == 2
    event = events[0]
    assert_event(event,
        location=u'Bedford Park Community Centre,81 Ranleigh Ave M4N 1X2',
        link=parent.link,
        location_latlon=parent.location_latlon,
        start_date=datetime(2016, 01, 16, 19, 35),
        end_date=datetime(2016, 01, 16, 21, 0),
        type=u'Leisure Swim')
    
def test_torontoparks_page_to_providers(loc2latlon):
    st = load_link('file:tst/toronto_parks_meta.html')
    now = datetime(2015, 12, 28, 8)
    providers = torontoparks_page_to_providers(st, now=now)
    assert len(providers) == 5
    assert_provider(providers[0],
                    link="http://www1.toronto.ca:80/parks/prd/facilities/complex/13/index.htm",
                    name="Adam Beck Community Centre",
                    location="79 Lawlor Ave, Toronto, ON",
                    location_latlon=GeoPt(43.6828607, -79.2888497),
                    processor=P_TORONTOPARKS)

def test_torontoparks_to_providers(loc2latlon):
    st = load_link('file:tst/toronto_parks_meta.html')
    now = datetime(2015, 12, 28, 8)
    providers = torontoparks_to_providers(st, url='file:tst/toronto_parks_meta.html', now=now)
    assert len(providers) == 10
    assert_provider(providers[0],
                    link="http://www1.toronto.ca:80/parks/prd/facilities/complex/13/index.htm",
                    name="Adam Beck Community Centre",
                    location="79 Lawlor Ave, Toronto, ON",
                    location_latlon=GeoPt(43.6828607, -79.2888497),
                    processor=P_TORONTOPARKS)
    assert_provider(providers[5],
                    link="http://www1.toronto.ca:80/parks/prd/facilities/complex/487/index.htm",
                    name="Ancaster Community Centre",
                    location="41 Ancaster Rd, Toronto, ON",
                    location_latlon=GeoPt(43.7324788, -79.4658635),
                    processor=P_TORONTOPARKS)

@pytest.fixture
def loc2latlon(request):
    patcher = patch('parsers.torontoparks.location_2_latlon')
    mock = patcher.start()
    def _loc2latlon(loc):
        if loc == "79 Lawlor Ave, Toronto, ON":
            return GeoPt(43.6828607, -79.2888497)
        if loc == "41 Ancaster Rd, Toronto, ON":
            return GeoPt(43.7324788, -79.4658635)
        return GeoPt(10, 10)
    mock.side_effect = _loc2latlon  
    def fin():
        patcher.stop()
    request.addfinalizer(fin)
    return mock
