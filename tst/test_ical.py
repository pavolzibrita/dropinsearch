from model.events import EventProvider
from parsers.icalparser import ical_to_events


def test_ical_parsing():
    st = open('tst/sample.ics').read()
    events = ical_to_events(st, EventProvider())
    assert len(events) == 50