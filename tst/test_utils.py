from datetime import datetime

from parsers.utils import get_start_end


def test_get_start_end_this_year():
    start, end = get_start_end("Sun Dec 27", "8am  -  1pm", "EST", now=datetime(2015, 12, 28, 00, 00))
    assert start == datetime(2015, 12, 27, 13, 00)
    assert end == datetime(2015, 12, 27, 18, 00)

def test_get_start_end_next_year():
    start, end = get_start_end("Sun Jan 27", "8am  -  1pm", "EST", now=datetime(2015, 12, 28, 00, 00))
    assert start == datetime(2016, 1, 27, 13, 00)
    assert end == datetime(2016, 1, 27, 18, 00)        

def test_get_start_end_leap_year():
    start, end = get_start_end("Sun Feb 29", "8am  -  1pm", "EST", now=datetime(2015, 12, 28, 00, 00))
    assert start == datetime(2016, 2, 29, 13, 00)
    assert end == datetime(2016, 2, 29, 18, 00)        

def test_get_start_end_incomplete():
    start, end = get_start_end("Sun Dec 27", "2  -  4pm", "EST", now=datetime(2015, 12, 28, 00, 00))
    assert start == datetime(2015, 12, 27, 19, 00)
    assert end == datetime(2015, 12, 27, 21, 00)        

def test_get_start_end_incomplete_2():
    start, end = get_start_end("Sat Feb 27", "9 - 10:15am", "EST", now=datetime(2015, 12, 28, 00, 00))
    assert start == datetime(2016, 2, 27, 14, 00)
    assert end == datetime(2016, 2, 27, 15, 15)        
