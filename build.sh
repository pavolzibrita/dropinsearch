#!/bin/sh

DOWNLOAD_DIR=./src
BUILD_DIR=./.env/src

# test for git and hg commands
for cmd in git hg; do
    command -v ${cmd} >/dev/null || { echo "sh: command not found: ${cmd}"; exit 1; }
done

rm lib -rf

virtualenv .env
. ./.env/bin/activate
pip install -r requirements.txt -t lib/ 
deactivate

rm -rf static/bootstrap
cp -r lib/flask_bootstrap/static static/bootstrap

[ -f ${DOWNLOAD_DIR}/pip-delete-this-directory.txt ] && rm -rf ${DOWNLOAD_DIR}

git checkout -- lib
